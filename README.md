<!-- markdownlint-configure-file { "MD004": { "style": "consistent" } } -->
<!-- markdownlint-disable MD033 -->
#

<p align="center">
    <a href="https://pi-hole.net/">
        <img src="https://pi-hole.github.io/graphics/Vortex/Vortex_with_Wordmark.svg" width="150" height="260" alt="Pi-hole">
    </a>
    <br>
    <strong>Network-wide ad blocking via your own Linux hardware</strong>
</p>
<!-- markdownlint-enable MD033 -->

The Pi-hole® is a [DNS sinkhole](https://en.wikipedia.org/wiki/DNS_Sinkhole) that protects your devices from unwanted content, without installing any client-side software.

  **Alpine version: x86 and x86_64**: This repository provides an automated script to install Pi-hole on Alpine Linux (32-bit and 64-bit) working with musl.
   The 32-bit version for Alpine compiles the [Faster-than-light Engine](https://github.com/pi-hole/ftl).
   More information is available at its own [repository](https://gitlab.com/yvelon/pihole-FTL-alpine).

   **Information about Pi-hole**: please check out the [Pi-hole official repository](https://github.com/pi-hole/pi-hole).

-----

## Installing

### On Alpine Linux

1. Prior to running, install `bash` and `git`.
2. Enable the _community_ repository by editing `/etc/apk/repositories`
3. Clone the current repository
`git clone https://gitlab.com/yvelon/pi-hole`
4. `cd` to the cloned folder
5. Allow the script to run on non-supported operating systems:
`export PIHOLE_SKIP_OS_CHECK=true`
6. Execute the script `bash automated install/basic_install.sh`

Note: installing Pi-hole on Alpine Linux x86 involves compiling the Faster-than-light Engine. The script takes care of that step automatically.

Note: Since Pi-hole is readily available for other Linux distributions, I have _only_ tested the installation on Alpine Linux x86 and x86_64.
